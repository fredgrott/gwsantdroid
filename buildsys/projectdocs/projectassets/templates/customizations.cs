<?cs # placeholder for custom clearsilver code. ?>
<?cs # project_nav() is the left nav bar for the project pages ?>
<?cs
def:project_nav() ?>
<div class="g-section g-tpl-240" id="body-content">
<div class="g-unit g-first" id="side-nav">
      <div id="devdoc-nav">
      <h2><span>Project Resources</span></h2>
<ul>
<li><a href="<?cs var:toroot ?>projectrepo.html"><span>ProjectRepo</span></a></li>
<li><a href="<?cs var:toroot ?>projectfeedback.html"><span>ProjectFeedback</span></a></li>
<li><a href="<?cs var:toroot ?>projectdemos.html"><span>ProjectDemos</span></a></li>
</ul>
</div>
</div>
</div>
<?cs /def ?>
<?cs # guitar_nav() is the left nav bar for the guitar pages ?>
<?cs
def:guitar_nav() ?>
<div class="g-section g-tpl-240" id="body-content">
<div class="g-unit g-first" id="side-nav">
    <div id="devdoc-nav">
    <h2><span>Guitar</span></h2>
 <ul>
 <li><a href="<?cs var:toroot ?>guitar/guitar.html"><span>Guitar</span></a></li>
 <li><a href="<?cs var:toroot ?>guitar/guitartests.html"><span>GuitarTests</span></a></li>
 </ul>
 </div>
 </div>
 </div>
 <?cs /def ?>
<?cs # javamonkey_nav() is the left nav bar for the javamonkey pages ?>
<?cs
def:javamonkey_nav() ?>
<div class="g-section g-tpl-240" id="body-content">
<div class="g-unit g-first" id="side-nav">
    <div id="devdoc-nav">
    <h2><span>JavaMonkey</span></h2>
 <ul>
 <li><a href="<?cs var:toroot ?>javamonkey/javamonkey.html"><span>JavaMonkey</span></a></li>
 <li><a href="</cs var:toroot ?>javamonkey/javamonkeytests.html"><span>JavaMonkeyTests</span></a></li>
 </ul>
 </div>
 </div>
 </div>
 <?cs /def ?>
 <?cs # instrumentedtesting_nav() is the left nav bar for the instrumentedtesting pages ?>
 <?cs
 def:instrumentedtesting_nav() ?>
 <div class="g-section g-tpl-240" id="body-content">
 <div class="g-unit g-first" id="side-nav">
    <div id="devdoc-nav">
    <h2><span>Instrumented</span></h2>
 <ul>
 <li><a href="<?cs var:toroot ?>instrumentedtesting/instrumentedtesting.html"><span>Instru</span></a></li>
 <li><a href="<?cs var:toroot ?>instrumentedtesting/emma.html"><span>EMMA</span></a></li>
 <li><a href="<?cs var:toroot ?>instrumentedtesting/junit.html"><span>JUnit</span></a></li>
 </ul>
 </div>
 </div>
 </div>
 <?cs /def ?>
 <?cs # ndk_nav() is the left nav bar for the ndk pages ?>
 <?cs
 def:ndk_nav() ?>
 <div class="g-section g-tpl-240" id="body-content">
 <div class="g-unit g-first" id="side-nav">
    <div id="devdoc-nav">
    <h2><span>NDK</span></h2>
 <ul>
 <li><a href="<?cs var:toroot ?>ndk/ndk.html"><span>NDK</span></a></li>
 <li><a href="<?cs var:toroot ?>ndk/ndkdocs.html"><span>NDKDocs</span></a></li>
 <li><a href="<?cs var:toroot ?>ndk/ndktests.html"><span>NDKTests</spann?</a></li>
 </ul>
 </div>
 </div>
 </div>
 <?cs /def ?>
 <?cs # webview_nav() is the left nav bar for the webview pages ?>
 <?cs
 def:webview_nav() ?>
 <div class="g-section g-tpl-240" id="body-content">
 <div class="g-unit g-first" id="side-nav">
        <div id="devdoc-nav">
        <h2><span>WebView</span></h2>
 <ul>
 <li><a href="<?cs var:toroot ?>webview/webview.html"><span>WebView</span></a></li>
 <li><a href="<?cs var:toroot ?>webview/webviewjsdocs.html"><span>JSDocs</span></a></li>
 <li><a href="<?cs var:toroot ?>webview/webviewjslint.html"><span>JSLint</span></a></li>
 <li><a href="<?cs var:toroot ?>webview/webviewjunit.html"><span>JUnit</span></a></li>
 </ul>
 </div>
 </div>
 </div>
 </cs /def ?>




<?cs # javaqa_nav() is the left nav bar for the javaqa pages ?>
<?cs
def:javaqa_nav() ?>
<div class="g-section g-tpl-240" id="body-content">
<div class="g-unit g-first" id="side-nav">
      <div id="devdoc-nav">
      <h2><span>JavaQA</span></h2>
<ul>
<li><a href="<?cs var:toroot ?>javaqa/androidlint.html"><span>AndroidLint</span></a></li>
<li><a href="<?cs var:toroot ?>javaqa/androidcheckstyle.html"><span>Checkstyle</span></a></li>
<li><a href="<?cs var:toroot ?>javaqa/androidpmd.html"><span>PMD</span></a></li>
<li><a href="<?cs var:toroot ?>javaqa/androidjavancss.html"><span>JavaNCSS</span></a></li>
<li><a href="<?cs var:toroot ?>javaqa/androidclassycle.html"><span>Classycle</span></a></li>
<li><a href="<?cs var:toroot ?>javaqa/androidjdepend.html"><span>JDepend</span></a></li>
<li><a href="<?cs var:toroot ?>javaqa/androiduml.html"><span>UML</span></a></li>
</ul>
</div>
</div>
</div>
<?cs /def ?>
<?cs # projectguide_nav() is the left nav bar for the projectguide pages ?>
<?cs
def:projectguide_nav() ?>
<div class="g-section g-tpl-240" id="body-content">
<div class="g-unit g-first" id="side-nav">
      <div id="devdoc-nav">
      <h2><span>ProjectGuide</span></h2>
<ul>
<li><a href="<?cs var:toroot ?>guidesetup.html"><span>SetUp</span></a></li>
<li><a href="<?cs var:toroot ?>guideusage.html"><span>Usage</span></a></li>
</ul>
</div>
</div>
</div>
<?cs /def ?>
<?cs # licensecredits_nav() is the lft nav bar for the licensecredits pages ?>
<?cs
def:licensecredits_nav() ?>
<div class="g-section g-tpl-240" id="body-content">
<div class="g-unit g-first" id="side-nav">
      <div id="devdoc-nav">
      <h2><span>License and Credits</span></h2>
<ul>
<li><a href="<?cs var:toroot ?>license.html"><span>License</span></a></li>
<li><a href="<?cs var:toroot ?>credits.html"><span>Credits</span></a></li>
</ul>
</div>
</div>
</div>
<?cs /def ?>






<?cs
def:custom_myleft_nav() ?><?cs
if:projectguide ?><?cs 
    call:projectguide_nav() ?><?cs 
  elif:credits ?><?cs 
    call:licensecredits_nav() ?><?cs 
  elif:javaqa ?><?cs 
    call:javaqa_nav() ?><?cs 
  elif:ndk ?><?cs
    call:ndk_nav() ?><?cs
  elif:webview ?><?cs
     call:webview_nav() ?><?cs
  elif:javamonkey ?><?cs
     call:javamonkey_nav() ?><?cs
  elif:guitar ?><?cs
     call:guitar_nav() ?><?cs
  elif:instrumentedtesting ?><?cs
      call:instrumentedtesting_nav() ?><?cs
  elif:project ?><?cs
     call:project_nav() ?><?cs  /if ?>
<?cs /def ?>


<?cs 
def:custom_copyright() ?>
  <p>Except as noted, this content and project(<?cs var:project.name ?>) is licensed under <a
  href="http://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a>. 
  For details and restrictions, see the <a href="<?cs var:toroot ?>license.html">
  Content License</a>.</p><?cs 
/def ?>
<?cs 
def:custom_footerlinks() ?>
  <p>
    <a href="http://shareme.github.com"><img src="<?cs var:toroot ?>assets/images/company_logo.png"/></a>
    <a href="https://bitbucket.org/fredgrott/activewallpaper">Project Code Repo</a>
    List at <a href="https://masterbranch.com/shareme">MasterBranch</a>
    Generated by <a href="http://code.google.com/p/doclava/">Doclava</a>.
    Icons from <a href="http://glyphicons.com">Glyphicons Free</a>, licensed under <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.
  </p><?cs 
/def ?>
<?cs # appears on the right side  at the bottom off every page ?><?cs 
def:custom_buildinfo() ?>
  Android API Level<?cs var:apilevel.top ?>&nbsp;downto&nbsp;<?cs var:apilevel.bottom ?> - <?cs var:page.now ?>
<?cs /def ?>
