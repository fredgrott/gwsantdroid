<div id="footer">
<div id="copyright">
    <?cs call:custom_copyright() ?>
  </div>
  <div id="build_info">
    <?cs call:custom_buildinfo() ?>
  </div>
  <div id="footerlinks">
    <?cs call:custom_footerlinks() ?>
  </div>.
</div> <!-- end footer -->
