<ul id="header-tabs" class="<?cs
if:home ?>home<?cs
elif:credits ?>credits<?cs
elif:reference ?>reference<?cs
elif:javaqa ?>javaqa<?cs
elif:webview ?>webivew<?cs
elif:instrumentedtesting ?>instrumentedtesting<?cs
elif:ndk ?>ndk<?cs
elfi:javamonkey ?>javamonkey<?cs
elif:guitar ?>guitar<?cs
elif:project ?>project<?cs
elif:projectguide ?>projectguide<?cs  /if ?>">
<li id="home-link"><a href="<?cs var:toroot?>index.html">
<span>Home</span>
</a></li>
<li id="credits-link"><a href="<?cs var:toroot ?>licensecredits.html">
<span>Credits</span>
</a></li>
<li id="reference-link"><a href="<?cs var:toroot ?>reference/packages.html">
<span>JavaRef</span>
</a></li>
<li id="javaqa-link"><a href="<?cs var:toroot ?>javaqa/javaqa.html">
<span>JavaQA</span>
</a></li>
<li id="webview-link"><a href="<?cs var:toroot ?>webiview/webview.html">
<span>Web</span>
</a></li>
<li id="instrumentedtesting-link"><a href="<?cs var:toroot ?>instrumentedtesting/instrumentedtesting.html">
<span>Instru</span>
</a></li>
<li id="ndk-link"><a href="<?cs var:toroot ?>ndk/ndk.html">
<span>Jni</span>
</a></li>
<li id="javamonkey-link"><a href="<?cs var:toroot ?>javamonkey/javamonkey.html">
<span>JavaMonkey</span>
</a></li>
<li id="projectguide-link"><a href="<?cs var:toroot ?>guide.html">
<span>Guide</span>
</a></li>
<li id="project-link"><a href="<?cs var:toroot ?>project.html">
<span>Project</span>
</a></li>
</ul>