GWSAntDroid
---

This will not FULLY FIX THE ANDROID APP ANT BUILD SYSTEM. But 
some small steps should be helpful in taking the pain out of the process.

# Different APPStores

Commmon code and common res files goes in Android Library Project, than
each app project using that APL represents an appstore or free, basic paid, 
or premium paid version. Obviously will not work for those containing 
native C code.

# ProjectDocs SetUp

The way it works is that the main app has the commmon template in the 
buildsys folder under projectdocs and that template assumes the 
mainapp way but the folder and link setup supports the other build 
project types. 

This means that you have slight modification to template content in 
other build project types but menus, links are setup to be less changes so
it works as a common template to all project build types which significantly 
reduces the build script logic code.

In other words when you setup instrumented test project type for example.
You will change the content of certain templates and in the menus hide 
certain links that do not apply, about 5 small changes total.

Ant var expansion in doclava, not possible so you set an ant var property 
instead in ant.properties  than:
<code>
  <?cs var:project.name ?>
</code>

to include the var value in a doclava/clearsiver template.

Of course in jd files you can only use java doclet vars as those get 
replaced by their values.

# GITIGNORE Setting

We add one folder to ignore beyond just bin and gen, the ext-libs folder
and the libs folder. Note, if your IDE android plugin adopts Google's 
put suportv4 in libs approach that is the lib that will be outside our 
depeendcy management system and for non Eclipse IDEs without this feature 
you need to supply this lib and manually add it along with the libs folder
when you git cloen projects set up this way. 

# Minimum Log Configuration 

Add the rollingFile logger to your log4j.properties file with the proper configuration 
thus you will than have a buildlog.log in a buildlogs sub folder of the bin directory.

I use default Log4j properties and loggers implemented through the Log4Ant and 
AntXtras ant task libs and thus at bare minium you need Log4J on the ant classpath 
which is provided by the project. If you need to use a different log system through 
SLF4j than you will replace that jar file with the one representing the log system 
you need to use.

Eclipse IDE users need to zip the log4j.properties file or the logback.xml file and rename 
it to a jar file and install it as an exernal jar in the ant runtime preferences otherwise you will 
get a build error, the default is to install the log4jproperties file that has been ziped 
and rename to a jar file.

# Minimum Dependency Management

This build system uses IVY dependency management and so you should load the 
minimum set of depedencies into your IVY cache on your machine. The easiest way to do 
that is to use the GWSAndroidProjectLibs bitbucket repo:

[GWSAndroidProjectLibs at Bitbucket]()

Just git clone it and run the build script on your dev machine.

# Project Modules

This individual set of ant build scripts is for the main App Project and also can be used for
an Instrumented Test Project Module or an Android Library Project. For the other 
project modules; Cucumber, JavaMonkey, and GUITAR the following bitbucket repos
contain those scripts:





You can also find an example of the Instrumented test project in GWSAntDroidTest:






